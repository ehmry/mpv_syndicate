# mpv Syndicate

A proof-of-concept for a new kind of
[plumbing](http://doc.cat-v.org/plan_9/4th_edition/papers/plumb).

The `mpv_syndicate` program starts an idle mpv instance and waits for assertions
that enqueue videos. When this program is renamed (symlinked to `mpv_syndicate`)
then it will enqueue each URI passed as command arguments.

Requires a local [Syndicate server](https://syndicate-lang.org/tools/broker/)
and is hardcoded to open a UNIX socket at `/run/syndicate/ds`.

```sh
nimble build # probably won't work, nimble is deprecated (unofficially)
ln -s mpv_syndicate enque_video

./enque_video file:///tmp/foo.mkv &
  # exits when all videos are enqueue

./mpv_syndicate
```
