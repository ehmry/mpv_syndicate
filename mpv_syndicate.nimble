# Package

version       = "0"
author        = "Emery Hemingway"
description   = "Syndicate wrapper for interacting with mpv"
license       = "Unlicense"
srcDir        = "src"
bin           = @["mpv_syndicate"]


# Dependencies

requires "nim >= 1.6.0", "syndicate"
